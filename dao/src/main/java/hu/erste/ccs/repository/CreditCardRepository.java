package hu.erste.ccs.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import hu.erste.ccs.domain.CreditCardEntity;

/**
 * Database Repository for {@link CreditCardEntity}.
 */
public interface CreditCardRepository extends CrudRepository<CreditCardEntity, Long> {
    /**
     * Returns the credit card for the specified card number.
     *
     * @param cardNumber The provided card number.
     * @return An optional {@link CreditCardEntity}
     */
    Optional<CreditCardEntity> findByCardNumber(String cardNumber);
}
