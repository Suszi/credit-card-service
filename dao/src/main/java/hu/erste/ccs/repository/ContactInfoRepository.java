package hu.erste.ccs.repository;

import org.springframework.data.repository.CrudRepository;

import hu.erste.ccs.domain.ContactInfoEntity;

/**
 * Database Repository for {@link ContactInfoEntity}.
 */
public interface ContactInfoRepository extends CrudRepository<ContactInfoEntity, Long> {
}
