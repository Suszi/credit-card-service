package hu.erste.ccs.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Represents a row in database's credit card table.
 */
@Entity
public class CreditCardEntity {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String cardType;
    private String cardNumber;
    private String validThru;
    private String owner;
    @OneToMany(mappedBy = "creditCard")
    private List<ContactInfoEntity> contactInfo;
    private boolean disabled;
    private int hash;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getValidThru() {
        return validThru;
    }

    public void setValidThru(String validThru) {
        this.validThru = validThru;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<ContactInfoEntity> getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(List<ContactInfoEntity> contactInfo) {
        this.contactInfo = contactInfo;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }
}
