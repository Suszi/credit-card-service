package hu.erste.ccs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * Starting point of the application.
 */
@SpringBootApplication
@ImportResource("classpath:application-context.xml")
public class CCSApplication {
    /**
     * Starting point of the application.
     *
     * @param args The arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(CCSApplication.class, args);
    }
}
