package hu.erste.ccs.facade;

import hu.erste.ccs.CreateCreditCardService;
import hu.erste.ccs.domain.CreditCard;
import hu.erste.ccs.domain.request.CreateCreditCardRequest;
import hu.erste.ccs.transformer.CreateCreditCardRequestTransformer;

/**
 * Facade for create credit card operation.
 */
public class CreateCreditCardFacade {
    private final CreateCreditCardRequestTransformer createCreditCardRequestTransformer;
    private final CreateCreditCardService createCreditCardService;

    CreateCreditCardFacade(CreateCreditCardRequestTransformer createCreditCardRequestTransformer, CreateCreditCardService createCreditCardService) {
        this.createCreditCardRequestTransformer = createCreditCardRequestTransformer;
        this.createCreditCardService = createCreditCardService;
    }

    /**
     * Transforms the request data into the proper format and calls the service to create a new credit card.
     *
     * @param createCreditCardRequest The {@link CreateCreditCardRequest}
     */
    public void create(CreateCreditCardRequest createCreditCardRequest) {
        CreditCard creditCard = createCreditCardRequestTransformer.transform(createCreditCardRequest);
        createCreditCardService.create(creditCard);
    }
}
