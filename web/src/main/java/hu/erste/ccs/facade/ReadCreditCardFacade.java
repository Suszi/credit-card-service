package hu.erste.ccs.facade;

import hu.erste.ccs.RetrieveCreditCardService;
import hu.erste.ccs.domain.CreditCard;
import hu.erste.ccs.domain.response.RetrieveCreditCardResponse;
import hu.erste.ccs.transformer.RetrieveCreditCardResponseTransformer;

/**
 * Facade for reading credit card operation.
 */
public class ReadCreditCardFacade {
    private final RetrieveCreditCardService retrieveCreditCardService;
    private final RetrieveCreditCardResponseTransformer retrieveCreditCardResponseTransformer;

    ReadCreditCardFacade(RetrieveCreditCardService retrieveCreditCardService, RetrieveCreditCardResponseTransformer retrieveCreditCardResponseTransformer) {
        this.retrieveCreditCardService = retrieveCreditCardService;
        this.retrieveCreditCardResponseTransformer = retrieveCreditCardResponseTransformer;
    }

    /**
     * Calls the service to read the credit card.
     *
     * @param cardNumber The number of the credit card
     * @return The {@link RetrieveCreditCardResponse}
     */
    public RetrieveCreditCardResponse retrieve(String cardNumber) {
        CreditCard creditCard = retrieveCreditCardService.retrieve(cardNumber);
        return retrieveCreditCardResponseTransformer.transform(creditCard);
    }
}
