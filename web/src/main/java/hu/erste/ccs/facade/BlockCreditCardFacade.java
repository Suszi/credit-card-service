package hu.erste.ccs.facade;

import hu.erste.ccs.BlockCreditCardService;

/**
 * Facade for block credit card operation.
 */
public class BlockCreditCardFacade {
    private final BlockCreditCardService blockCreditCardService;

    BlockCreditCardFacade(BlockCreditCardService blockCreditCardService) {
        this.blockCreditCardService = blockCreditCardService;
    }

    /**
     * Mars a credit card with the provided card number as blocked.
     *
     * @param cardNumber The number of the card.
     */
    public void block(String cardNumber) {
        blockCreditCardService.block(cardNumber);
    }
}
