package hu.erste.ccs.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.erste.ccs.ValidateCreditCardService;
import hu.erste.ccs.domain.request.ValidateCreditCardRequest;
import hu.erste.ccs.domain.response.ValidateCreditCardResponse;
import hu.erste.ccs.factory.ValidateCreditCardResponseFactory;

/**
 * Facade for validate credit card operation.
 */
public class ValidateCreditCardFacade {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidateCreditCardFacade.class);

    private final ValidateCreditCardService validateCreditCardService;
    private final ValidateCreditCardResponseFactory validateCreditCardResponseFactory;

    ValidateCreditCardFacade(ValidateCreditCardService validateCreditCardService, ValidateCreditCardResponseFactory validateCreditCardResponseFactory) {
        this.validateCreditCardService = validateCreditCardService;
        this.validateCreditCardResponseFactory = validateCreditCardResponseFactory;
    }

    /**
     * Validates a credit card.
     *
     * @param validateCreditCardRequest The {@link ValidateCreditCardRequest}
     * @return The {@link ValidateCreditCardResponse}
     */
    public ValidateCreditCardResponse validate(ValidateCreditCardRequest validateCreditCardRequest) {
        String validationResult =
                validateCreditCardService.validate(validateCreditCardRequest.getCardType(), validateCreditCardRequest.getCardNumber(), validateCreditCardRequest.getValidThru(),
                        validateCreditCardRequest.getCvv());
        LOGGER.info("Validation result for card number: {} is {}", validateCreditCardRequest.getCardNumber(), validationResult);
        return validateCreditCardResponseFactory.create(validationResult);
    }
}
