package hu.erste.ccs.transformer;

import java.util.List;

import hu.erste.ccs.CreditCardHashGenerator;
import hu.erste.ccs.domain.ContactInfo;
import hu.erste.ccs.domain.CreditCard;
import hu.erste.ccs.domain.request.CreateCreditCardRequest;

/**
 * Transforms a {@link CreateCreditCardRequest} into a {@link CreditCard}.
 */
public class CreateCreditCardRequestTransformer {
    private final ContactInfoRequestTransformer contactInfoRequestTransformer;
    private final CreditCardHashGenerator creditCardHashGenerator;

    CreateCreditCardRequestTransformer(ContactInfoRequestTransformer contactInfoRequestTransformer, CreditCardHashGenerator creditCardHashGenerator) {
        this.contactInfoRequestTransformer = contactInfoRequestTransformer;
        this.creditCardHashGenerator = creditCardHashGenerator;
    }

    /**
     * Transforms a {@link CreateCreditCardRequest} into a {@link CreditCard}.
     *
     * @param createCreditCardRequest The {@link CreateCreditCardRequest}
     * @return The transformed {@link CreditCard}
     */
    public CreditCard transform(CreateCreditCardRequest createCreditCardRequest) {
        List<ContactInfo> contactInfoList = contactInfoRequestTransformer.transform(createCreditCardRequest.getContactInfo());
        int hash = creditCardHashGenerator.generate(createCreditCardRequest.getCardNumber(), createCreditCardRequest.getValidThru(), createCreditCardRequest.getCvv());
        return new CreditCard.Builder().withCardNumber(createCreditCardRequest.getCardNumber())
                .withCardType(createCreditCardRequest.getCardType())
                .withContactInfo(contactInfoList)
                .withCvv(createCreditCardRequest.getCvv())
                .withOwner(createCreditCardRequest.getOwner())
                .withValidThru(createCreditCardRequest.getValidThru())
                .withHash(hash)
                .build();
    }
}
