package hu.erste.ccs.transformer;

import hu.erste.ccs.domain.CreditCard;
import hu.erste.ccs.domain.response.RetrieveCreditCardResponse;

/**
 * Transforms a {@link CreditCard} into a {@link RetrieveCreditCardResponse}.
 */
public class RetrieveCreditCardResponseTransformer {
    private final ContactInfoResponseTransformer contactInfoResponseTransformer;

    RetrieveCreditCardResponseTransformer(ContactInfoResponseTransformer contactInfoResponseTransformer) {
        this.contactInfoResponseTransformer = contactInfoResponseTransformer;
    }

    /**
     * Transforms a {@link CreditCard} into a {@link RetrieveCreditCardResponse}.
     *
     * @param creditCard The {@link CreditCard}
     * @return The transformed {@link RetrieveCreditCardResponse}
     */
    public RetrieveCreditCardResponse transform(CreditCard creditCard) {
        return new RetrieveCreditCardResponse.Builder().withCardNumber(creditCard.getCardNumber())
                .withCardType(creditCard.getCardType())
                .withCardNumber(creditCard.getCardNumber())
                .withValidThru(creditCard.getValidThru())
                .withDisabled(creditCard.isDisabled())
                .withOwner(creditCard.getOwner())
                .withContactInfo(contactInfoResponseTransformer.transform(creditCard.getContactInfo()))
                .build();
    }
}
