package hu.erste.ccs.transformer;

import java.util.List;
import java.util.stream.Collectors;

import hu.erste.ccs.domain.ContactInfo;
import hu.erste.ccs.domain.request.ContactInfoRequest;

/**
 * Transforms a {@link ContactInfoRequest} into a {@link ContactInfo}.
 */
public class ContactInfoRequestTransformer {

    List<ContactInfo> transform(List<ContactInfoRequest> contactInforRequests) {
        return contactInforRequests.stream()
                .map(contactInfoRequest -> createContactInfo(contactInfoRequest.getType(), contactInfoRequest.getContact()))
                .collect(Collectors.toList());
    }

    private ContactInfo createContactInfo(String type, String contact) {
        return new ContactInfo(type, contact);
    }
}
