package hu.erste.ccs.transformer;

import java.util.List;
import java.util.stream.Collectors;

import hu.erste.ccs.domain.response.ContactInfo;

/**
 * Transform a list of {@link hu.erste.ccs.domain.ContactInfo} into a list of {@link ContactInfo}.
 */
public class ContactInfoResponseTransformer {
    /**
     * Transform a list of {@link hu.erste.ccs.domain.ContactInfo} into a list of {@link ContactInfo}.
     *
     * @param contactInfoList The of {@link hu.erste.ccs.domain.ContactInfo}
     * @return The transformed list of {@link ContactInfo}
     */
    public List<ContactInfo> transform(List<hu.erste.ccs.domain.ContactInfo> contactInfoList) {
        return contactInfoList.stream()
                .map(this::transformContactInfo)
                .collect(Collectors.toList());
    }

    private ContactInfo transformContactInfo(hu.erste.ccs.domain.ContactInfo contactInfo) {
        return new ContactInfo(contactInfo.getType(), contactInfo.getContact());
    }
}
