package hu.erste.ccs.domain.response;

/**
 * Contains contact information.
 */
public class ContactInfo {
    private final String type;
    private final String contact;

    /**
     * Constructor.
     *
     * @param type    The type of the contact
     * @param contact The contact data;
     */
    public ContactInfo(String type, String contact) {
        this.type = type;
        this.contact = contact;
    }

    public String getType() {
        return type;
    }

    public String getContact() {
        return contact;
    }
}
