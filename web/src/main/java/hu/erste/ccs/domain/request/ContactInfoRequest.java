package hu.erste.ccs.domain.request;

import javax.validation.constraints.NotBlank;

/**
 * Represents the contact information in the request of the create credit card operation.
 */
public class ContactInfoRequest {
    @NotBlank
    private String type;
    @NotBlank
    private String contact;

    public ContactInfoRequest(String type, String contact) {
        this.type = type;
        this.contact = contact;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
