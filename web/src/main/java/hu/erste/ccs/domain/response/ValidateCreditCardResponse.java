package hu.erste.ccs.domain.response;

/**
 * Represents the response of a validate credit card call.
 */
public class ValidateCreditCardResponse {
    private final String result;

    /**
     * Constructor.
     *
     * @param result The result of the validation
     */
    public ValidateCreditCardResponse(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
