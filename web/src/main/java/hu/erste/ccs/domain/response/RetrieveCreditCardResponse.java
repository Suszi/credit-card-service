package hu.erste.ccs.domain.response;

import java.util.List;

/**
 * Represents the response of a retrieve credit card call.
 */
public final class RetrieveCreditCardResponse {
    private final String cardType;
    private final String cardNumber;
    private final String validThru;
    private final boolean disabled;
    private final String owner;
    private final List<ContactInfo> contactInfo;

    private RetrieveCreditCardResponse(Builder builder) {
        this.cardType = builder.cardType;
        this.cardNumber = builder.cardNumber;
        this.validThru = builder.validThru;
        this.disabled = builder.disabled;
        this.owner = builder.owner;
        this.contactInfo = builder.contactInfo;
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getValidThru() {
        return validThru;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public String getOwner() {
        return owner;
    }

    public List<ContactInfo> getContactInfo() {
        return contactInfo;
    }

    /**
     * Builder class for {@link RetrieveCreditCardResponse}.
     */
    public static class Builder {
        private String cardType;
        private String cardNumber;
        private String validThru;
        private boolean disabled;
        private String owner;
        private List<ContactInfo> contactInfo;

        public Builder withCardType(String cardType) {
            this.cardType = cardType;
            return this;
        }

        public Builder withCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public Builder withValidThru(String validThru) {
            this.validThru = validThru;
            return this;
        }

        public Builder withDisabled(boolean disabled) {
            this.disabled = disabled;
            return this;
        }

        public Builder withOwner(String owner) {
            this.owner = owner;
            return this;
        }

        public Builder withContactInfo(List<ContactInfo> contactInfo) {
            this.contactInfo = contactInfo;
            return this;
        }

        /**
         * Creates a new instance of {@link RetrieveCreditCardResponse}.
         *
         * @return the newly created {@link RetrieveCreditCardResponse}
         */
        public RetrieveCreditCardResponse build() {
            return new RetrieveCreditCardResponse(this);
        }
    }
}
