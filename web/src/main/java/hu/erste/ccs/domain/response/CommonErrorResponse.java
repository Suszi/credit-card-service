package hu.erste.ccs.domain.response;

/**
 * Use for Common error responses for the controllers.
 */
public class CommonErrorResponse {

    private final String message;

    public CommonErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
