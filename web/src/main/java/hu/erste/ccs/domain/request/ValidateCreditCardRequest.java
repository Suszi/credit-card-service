package hu.erste.ccs.domain.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.CreditCardNumber;

/**
 * Represents the request of the validate credit card operation.
 */
public class ValidateCreditCardRequest {
    @NotBlank
    private final String cardType;
    @NotBlank
    @CreditCardNumber
    private final String cardNumber;
    @NotNull
    @Pattern(regexp = "\\d\\d/\\d\\d")
    private final String validThru;
    @NotNull
    @Pattern(regexp = "\\d\\d\\d")
    private final String cvv;

    public ValidateCreditCardRequest(String cardType, String cardNumber, String validThru, String cvv) {
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.validThru = validThru;
        this.cvv = cvv;
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getValidThru() {
        return validThru;
    }

    public String getCvv() {
        return cvv;
    }
}
