package hu.erste.ccs.domain.request;

import java.util.Collections;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import org.hibernate.validator.constraints.CreditCardNumber;

/**
 * Represents the request of the create credit card operation.
 */
@JsonDeserialize(builder = CreateCreditCardRequest.Builder.class)
public final class CreateCreditCardRequest {
    public static final int MIN_LENGTH = 1;
    public static final int MAX_LENGTH = 50;

    @NotBlank
    private final String cardType;
    @NotBlank
    @CreditCardNumber
    private final String cardNumber;
    @NotNull
    @Pattern(regexp = "\\d\\d/\\d\\d")
    private final String validThru;
    @NotNull
    @Pattern(regexp = "\\d\\d\\d")
    private final String cvv;
    @NotBlank
    @Size(min = MIN_LENGTH, max = MAX_LENGTH)
    private final String owner;
    @NotEmpty
    private final List<ContactInfoRequest> contactInfo;

    private CreateCreditCardRequest(Builder builder) {
        this.cardType = builder.cardType;
        this.cardNumber = builder.cardNumber;
        this.validThru = builder.validThru;
        this.cvv = builder.cvv;
        this.owner = builder.owner;
        this.contactInfo = builder.contactInfo != null ? Collections.unmodifiableList(builder.contactInfo) : Collections.emptyList();
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getValidThru() {
        return validThru;
    }

    public String getCvv() {
        return cvv;
    }

    public String getOwner() {
        return owner;
    }

    public List<ContactInfoRequest> getContactInfo() {
        return contactInfo;
    }

    /**
     * Builder class for {@link CreateCreditCardRequest}.
     */
    public static class Builder {
        private String cardType;
        private String cardNumber;
        private String validThru;
        private String cvv;
        private String owner;
        private List<ContactInfoRequest> contactInfo;

        public Builder withCardType(String cardType) {
            this.cardType = cardType;
            return this;
        }

        public Builder withCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public Builder withValidThru(String validThru) {
            this.validThru = validThru;
            return this;
        }

        public Builder withCvv(String cvv) {
            this.cvv = cvv;
            return this;
        }

        public Builder withOwner(String owner) {
            this.owner = owner;
            return this;
        }

        public Builder withContactInfo(List<ContactInfoRequest> contactInfo) {
            this.contactInfo = contactInfo;
            return this;
        }

        /**
         * Builds an instance of {@link CreateCreditCardRequest}.
         *
         * @return a {@link CreateCreditCardRequest}
         */
        public CreateCreditCardRequest build() {
            return new CreateCreditCardRequest(this);
        }
    }
}
