package hu.erste.ccs.factory;

import hu.erste.ccs.domain.response.ValidateCreditCardResponse;

/**
 * Factory for creating {@link ValidateCreditCardResponse}.
 */
public class ValidateCreditCardResponseFactory {
    /**
     * Creates a new instance of {@link ValidateCreditCardResponse}.
     *
     * @param result The result
     * @return The newly created {@link ValidateCreditCardResponse}
     */
    public ValidateCreditCardResponse create(String result) {
        return new ValidateCreditCardResponse(result);
    }
}
