package hu.erste.ccs.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import hu.erste.ccs.domain.request.CreateCreditCardRequest;
import hu.erste.ccs.domain.request.ValidateCreditCardRequest;
import hu.erste.ccs.domain.response.RetrieveCreditCardResponse;
import hu.erste.ccs.domain.response.ValidateCreditCardResponse;
import hu.erste.ccs.facade.BlockCreditCardFacade;
import hu.erste.ccs.facade.CreateCreditCardFacade;
import hu.erste.ccs.facade.ReadCreditCardFacade;
import hu.erste.ccs.facade.ValidateCreditCardFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Controller class for handling credit card related actions.
 */
@RequestMapping
public class CreditCardController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardController.class);
    private static final String APPLICATION_JSON = "application/json";

    private final CreateCreditCardFacade createCreditCardFacade;
    private final ReadCreditCardFacade readCreditCardFacade;
    private final ValidateCreditCardFacade validateCreditCardFacade;
    private final BlockCreditCardFacade blockCreditCardFacade;

    CreditCardController(CreateCreditCardFacade createCreditCardFacade, ReadCreditCardFacade readCreditCardFacade, ValidateCreditCardFacade validateCreditCardFacade,
                         BlockCreditCardFacade blockCreditCardFacade) {
        this.createCreditCardFacade = createCreditCardFacade;
        this.readCreditCardFacade = readCreditCardFacade;
        this.validateCreditCardFacade = validateCreditCardFacade;
        this.blockCreditCardFacade = blockCreditCardFacade;
    }

    /**
     * Creates a new credit card and inserts it into the database if the request is valid.
     *
     * @param createCreditCardRequest Contains the data sent by client in the payload
     * @return The {@link ResponseEntity}
     */
    @Operation(summary = "Creating a new credit card.")
    @ApiResponses({@ApiResponse(responseCode = "200", description = "Credit card has been created!",
            content = {@Content(mediaType = APPLICATION_JSON, schema = @Schema(implementation = ResponseEntity.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error!")})
    @PostMapping("${request-mappings.create}")
    @ResponseBody
    public ResponseEntity<HttpStatus> create(@RequestBody @Valid CreateCreditCardRequest createCreditCardRequest) {
        LOGGER.info("Create credit card endpoint has been called!");
        createCreditCardFacade.create(createCreditCardRequest);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    /**
     * Returns the credit card based on the input data.
     *
     * @param cardNumber The credit card number
     * @return The {@link RetrieveCreditCardResponse}
     */
    @Operation(summary = "Retrieve a credit card.")
    @ApiResponses({@ApiResponse(responseCode = "200", description = "The credit card has been retrieved!",
            content = {@Content(mediaType = APPLICATION_JSON, schema = @Schema(implementation = RetrieveCreditCardResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Credit card not found!")})
    @GetMapping(path = "${request-mappings.read}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public RetrieveCreditCardResponse retrieve(@PathVariable String cardNumber) {
        LOGGER.info("Read credit card endpoint has been called!");
        return readCreditCardFacade.retrieve(cardNumber);
    }

    /**
     * Validates a credit card based on the intput and stored data.
     *
     * @param validateCreditCardRequest The {@link ValidateCreditCardRequest}
     * @return The {@link ValidateCreditCardResponse}
     */
    @Operation(summary = "Validating a credit card.")
    @ApiResponses({@ApiResponse(responseCode = "200", description = "Credit card has been validated!",
            content = {@Content(mediaType = APPLICATION_JSON, schema = @Schema(implementation = ValidateCreditCardResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error!")})
    @PostMapping("${request-mappings.validate}")
    @ResponseBody
    public ValidateCreditCardResponse validate(@RequestBody @Valid ValidateCreditCardRequest validateCreditCardRequest) {
        LOGGER.info("Validate credit card endpoint has been called!");
        return validateCreditCardFacade.validate(validateCreditCardRequest);
    }

    /**
     * Blocks a credit card.
     *
     * @param cardNumber The number of the card
     * @return The {@link ResponseEntity}
     */
    @Operation(summary = "Blocks a credit card.")
    @ApiResponses({@ApiResponse(responseCode = "200", description = "Credit card has been blocked!",
            content = {@Content(mediaType = APPLICATION_JSON, schema = @Schema(implementation = ResponseEntity.class))}),
            @ApiResponse(responseCode = "404", description = "Credit card not found!")})
    @PutMapping(path = "${request-mappings.block}")
    @ResponseBody
    public ResponseEntity<HttpStatus> block(@PathVariable("cardNumber") String cardNumber) {
        LOGGER.info("Block credit card endpoint has been called!");
        blockCreditCardFacade.block(cardNumber);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
