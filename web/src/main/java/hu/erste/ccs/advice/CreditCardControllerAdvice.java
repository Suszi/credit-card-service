package hu.erste.ccs.advice;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import hu.erste.ccs.controller.CreditCardController;
import hu.erste.ccs.domain.response.CommonErrorResponse;

/**
 * Error handler class for {@link CreditCardController}.
 * Handles Exceptions to return proper response to the client.
 */
@ControllerAdvice(basePackageClasses = CreditCardController.class)
public class CreditCardControllerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardControllerAdvice.class);
    private static final String COMMON_ERROR_LOG_PATTERN = "Error occurred while during user operation: {}";
    private static final String COMMON_ERROR_MESSAGE = "Internal server error happened!";
    private static final String ENTITY_NOT_FOUND_ERROR_MESSAGE = "Entity not found!";

    @ExceptionHandler(Exception.class)
    @ResponseStatus(code = INTERNAL_SERVER_ERROR)
    @ResponseBody
    CommonErrorResponse handleException(Exception exception) {
        LOGGER.error(COMMON_ERROR_LOG_PATTERN, exception);
        return new CommonErrorResponse(COMMON_ERROR_MESSAGE);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(code = NOT_FOUND)
    @ResponseBody
    CommonErrorResponse handleEntityNotFoundException(EntityNotFoundException entityNotFoundException) {
        LOGGER.error(ENTITY_NOT_FOUND_ERROR_MESSAGE, entityNotFoundException);
        return new CommonErrorResponse(COMMON_ERROR_MESSAGE);
    }
}
