# About Credit Card Service

**CCS** is a test project for ERSTE Bank Senior Java Developer position

# Project specifications

## Functional requirements

Store credit card data in database
* Card type - https://www.erstebank.hu/hu/ebh-nyito/mindennapi-penzugyek/beteti-kartyak
* Card number
* Validity in MM/YY format
* Hash from card number, validity date, CVV
* Blocking flag
* Owner's name
* List of contact details
    * Type of contact (EMAIL/SMS)
    * Availability

### REST Endpoints

#### Register new card

Endpoint url: /ecards

Endpoint type: POST

Input type JSON.

In case of a successful run the application returns HTTP 200.

In case of error the application returns with HTTP 500. 

##### Parameters

| Level | Parameter | Data type | Description |
| ----- | ----- | ----- | ----- |
| 1 | cardType | String | Type of the card |
| 1 | cardNumber | String | Card number |
| 1 | validThru | String | Validity date |
| 1 | CVV | String | CVV number |
| 1 | owner | String | Name of the owner |
| 1 | contactInfo | Array | Contact information |
| 2 | type | String | Type of the contact |
| 2 | contact | String | Contact data |

#### Read card

Endpoint url: /ecards/{cardNumber}

Endpoint type: GET

In case of a successful run the application returns with the card data except the hash.

In case of error the application returns with HTTP 404. 

##### Parameters

| Level | Parameter | Data type | Description |
| ----- | ----- | ----- | ----- |
| 1 | cardType | String | Type of the card |
| 1 | cardNumber | String | Card number |
| 1 | validThru | String | Validity date |
| 1 | disabled | boolean | True if the card is blocked |
| 1 | owner | String | Name of the owner |
| 1 | contactInfo | Array | Contact information |
| 2 | type | String | Type of the contact |
| 2 | contact | String | Contact data |

#### Validate card

Endpoint url: /ecards/validate

Endpoint type: POST

Request and response type are both JSON.

The validations are:
* The input data is the same as the stored data.
* The credit card is not disabled.
* The stored hash is the same as the calculated hash based on the request data.

The credit card is valid if all the validations are valid. Any other case the credit card is invalid.

If the input data is invalid than the response code is HTTP 500. 

##### Parameters

| Level | Parameter | Data type | Description |
| ----- | ----- | ----- | ----- |
| 1 | cardType | String | Type of the card |
| 1 | cardNumber | String | Card number |
| 1 | validThru | String | Validity date |
| 1 | CVV | String | CVV number |

##### Response structure

| Level | Parameter | Data type | Description|
| ----- | ----- | ----- | ----- |
| 1 | result | String | The result of the validation VALID / INVALID |

#### Block card

Endpoint: /ecards/{cardNumber}

Endpoint type: PUT

In case of a successful run the application returns with HTTP 200.

In case of error the application returns with HTTP 404.

# How to run Credit Card Service

1. Navigate into the `root` folder
2. Build the application with `mvn clean install` command
3. Navigate into the `release` folder
4. Run the application with `mvn spring-boot:run -Dspring.profiles.active=dev,h2`
5. Open postman and import collection from `src/site/resources/postman` folder

# How to access database

This applciation is using H2 in memory DB.
After the application started successbully you can check the content of the db if you type `http://localhost:8321/h2-console` in any browser.

These are the parameters:
* Driver class: org.h2.Driver
* JDBC URL: jdbc:h2:mem:test
* User name: ccs
* Password: leave empty