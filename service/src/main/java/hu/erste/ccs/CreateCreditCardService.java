package hu.erste.ccs;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.erste.ccs.domain.ContactInfo;
import hu.erste.ccs.domain.ContactInfoEntity;
import hu.erste.ccs.domain.CreditCard;
import hu.erste.ccs.domain.CreditCardEntity;
import hu.erste.ccs.repository.ContactInfoRepository;
import hu.erste.ccs.repository.CreditCardRepository;

/**
 * Service for creating a new credit card.
 */
public class CreateCreditCardService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateCreditCardService.class);

    private final CreditCardRepository creditCardRepository;
    private final ContactInfoRepository contactInfoRepository;

    CreateCreditCardService(CreditCardRepository creditCardRepository, ContactInfoRepository contactInfoRepository) {
        this.creditCardRepository = creditCardRepository;
        this.contactInfoRepository = contactInfoRepository;
    }

    /**
     * Creates a new {@link CreditCardEntity} and saves in the database.
     *
     * @param creditCard The credit card data
     */
    public void create(CreditCard creditCard) {
        LOGGER.info("CreateCreditCardService create has been called.");
        CreditCardEntity creditCardEntity = new CreditCardEntity();
        creditCardEntity.setCardNumber(creditCard.getCardNumber());
        creditCardEntity.setCardType(creditCard.getCardType());
        creditCardEntity.setOwner(creditCard.getOwner());
        creditCardEntity.setValidThru(creditCard.getValidThru());
        creditCardEntity.setHash(creditCard.getHash());
        creditCardEntity = creditCardRepository.save(creditCardEntity);
        addContactInfo(creditCardEntity, creditCard.getContactInfo());
    }

    private void addContactInfo(CreditCardEntity creditCardEntity, List<ContactInfo> contactInfo) {
        List<ContactInfoEntity> contactInfoEntities = createContactInfoEntities(contactInfo, creditCardEntity);
        creditCardEntity.setContactInfo(contactInfoEntities);
        creditCardRepository.save(creditCardEntity);
    }

    private List<ContactInfoEntity> createContactInfoEntities(List<ContactInfo> contactInfo, CreditCardEntity creditCardEntity) {
        return contactInfo.stream()
                .map(contactInfoRequest -> createContactInfoEntity(contactInfoRequest.getType(), contactInfoRequest.getContact(), creditCardEntity))
                .collect(Collectors.toList());
    }

    private ContactInfoEntity createContactInfoEntity(String type, String contact, CreditCardEntity creditCardEntity) {
        ContactInfoEntity contactInfoEntity = new ContactInfoEntity();
        contactInfoEntity.setType(type);
        contactInfoEntity.setContact(contact);
        contactInfoEntity.setCreditCard(creditCardEntity);
        contactInfoEntity = contactInfoRepository.save(contactInfoEntity);
        return contactInfoEntity;
    }
}
