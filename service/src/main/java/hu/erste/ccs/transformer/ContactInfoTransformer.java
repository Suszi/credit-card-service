package hu.erste.ccs.transformer;

import java.util.List;
import java.util.stream.Collectors;

import hu.erste.ccs.domain.ContactInfo;
import hu.erste.ccs.domain.ContactInfoEntity;

/**
 * Transformer a list of {@link ContactInfoEntity} into a list of {@link ContactInfo}.
 */
public class ContactInfoTransformer {

    List<ContactInfo> transform(List<ContactInfoEntity> contactInfoEntities) {
        return contactInfoEntities.stream()
                .map(this::transformContactInfo)
                .collect(Collectors.toList());
    }

    private ContactInfo transformContactInfo(ContactInfoEntity contactInfoEntity) {
        return new ContactInfo(contactInfoEntity.getType(), contactInfoEntity.getContact());
    }
}
