package hu.erste.ccs.transformer;

import hu.erste.ccs.domain.CreditCard;
import hu.erste.ccs.domain.CreditCardEntity;

/**
 * Transformer a {@link CreditCardEntity} into a {@link CreditCard}.
 */
public class CreditCardTransformer {
    private final ContactInfoTransformer contactInfoTransformer;

    CreditCardTransformer(ContactInfoTransformer contactInfoTransformer) {
        this.contactInfoTransformer = contactInfoTransformer;
    }

    /**
     * Transforms {@link CreditCardEntity} to {@link CreditCard}.
     *
     * @param creditCardEntity The {@link CreditCardEntity}
     * @return The transformed {@link CreditCard}
     */
    public CreditCard transform(CreditCardEntity creditCardEntity) {
        return new CreditCard.Builder().withCardType(creditCardEntity.getCardType())
                .withCardNumber(creditCardEntity.getCardNumber())
                .withValidThru(creditCardEntity.getValidThru())
                .withOwner(creditCardEntity.getOwner())
                .withContactInfo(contactInfoTransformer.transform(creditCardEntity.getContactInfo()))
                .withDisabled(creditCardEntity.isDisabled())
                .withHash(creditCardEntity.getHash())
                .build();
    }
}
