package hu.erste.ccs.domain;

import java.util.Collections;
import java.util.List;

/**
 * Represents the request of the create credit card operation.
 */
public final class CreditCard {
    private final String cardType;
    private final String cardNumber;
    private final String validThru;
    private final String cvv;
    private final String owner;
    private final List<ContactInfo> contactInfo;
    private final int hash;
    private final boolean disabled;

    private CreditCard(Builder builder) {
        this.cardType = builder.cardType;
        this.cardNumber = builder.cardNumber;
        this.validThru = builder.validThru;
        this.cvv = builder.cvv;
        this.owner = builder.owner;
        this.contactInfo = builder.contactInfo != null ? Collections.unmodifiableList(builder.contactInfo) : Collections.emptyList();
        this.hash = builder.hash;
        this.disabled = builder.disabled;
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getValidThru() {
        return validThru;
    }

    public String getCvv() {
        return cvv;
    }

    public String getOwner() {
        return owner;
    }

    public List<ContactInfo> getContactInfo() {
        return contactInfo;
    }

    public int getHash() {
        return hash;
    }

    public boolean isDisabled() {
        return disabled;
    }

    /**
     * Builder class for {@link CreditCard}.
     */
    public static class Builder {
        private String cardType;
        private String cardNumber;
        private String validThru;
        private String cvv;
        private String owner;
        private List<ContactInfo> contactInfo;
        private int hash;
        private boolean disabled;

        public Builder withCardType(String cardType) {
            this.cardType = cardType;
            return this;
        }

        public Builder withCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public Builder withValidThru(String validThru) {
            this.validThru = validThru;
            return this;
        }

        public Builder withCvv(String cvv) {
            this.cvv = cvv;
            return this;
        }

        public Builder withOwner(String owner) {
            this.owner = owner;
            return this;
        }

        public Builder withContactInfo(List<ContactInfo> contactInfo) {
            this.contactInfo = contactInfo;
            return this;
        }

        public Builder withHash(int hash) {
            this.hash = hash;
            return this;
        }

        public Builder withDisabled(boolean disabled) {
            this.disabled = disabled;
            return this;
        }

        /**
         * Builds an instance of {@link CreditCard}.
         *
         * @return a {@link CreditCard}
         */
        public CreditCard build() {
            return new CreditCard(this);
        }
    }
}
