package hu.erste.ccs;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.erste.ccs.domain.CreditCard;
import hu.erste.ccs.domain.CreditCardEntity;
import hu.erste.ccs.repository.CreditCardRepository;
import hu.erste.ccs.transformer.CreditCardTransformer;

/**
 * Service for retrieving a credit card.
 */
public class RetrieveCreditCardService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RetrieveCreditCardService.class);

    private final CreditCardRepository creditCardRepository;
    private final CreditCardTransformer creditCardTransformer;

    RetrieveCreditCardService(CreditCardRepository creditCardRepository, CreditCardTransformer creditCardTransformer) {
        this.creditCardRepository = creditCardRepository;
        this.creditCardTransformer = creditCardTransformer;
    }

    /**
     * Returns the retrieved credit card based on the input parameter.
     *
     * @param cardNumber The number of the credit card
     * @return The retrieved credit card
     */
    public CreditCard retrieve(String cardNumber) {
        LOGGER.info("RetrieveCreditCardService retrieve has been called.");
        Optional<CreditCardEntity> optionalCreditCardEntity = creditCardRepository.findByCardNumber(cardNumber);
        return optionalCreditCardEntity.map(creditCardTransformer::transform)
                .orElseThrow(EntityNotFoundException::new);
    }
}
