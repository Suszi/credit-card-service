package hu.erste.ccs;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.erste.ccs.domain.CreditCardEntity;
import hu.erste.ccs.repository.CreditCardRepository;

/**
 * Service for validating a credit card.
 */
public class ValidateCreditCardService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidateCreditCardService.class);

    public static final String INVALID = "INVALID";
    public static final String VALID = "VALID";

    private final CreditCardRepository creditCardRepository;
    private final CreditCardHashGenerator creditCardHashGenerator;


    ValidateCreditCardService(CreditCardRepository creditCardRepository, CreditCardHashGenerator creditCardHashGenerator) {
        this.creditCardRepository = creditCardRepository;
        this.creditCardHashGenerator = creditCardHashGenerator;
    }

    /**
     * Validates a credit card based on the input parameters.
     *
     * @param cardType   The type of the credit card
     * @param cardNumber The number of the credit card
     * @param validThru  The validation date
     * @param cvv        The cvv
     * @return The result of the validation
     */
    public String validate(String cardType, String cardNumber, String validThru, String cvv) {
        LOGGER.info("ValidateCreditCardService validate has been called.");
        Optional<CreditCardEntity> optionalCreditCardEntity = creditCardRepository.findByCardNumber(cardNumber);
        return optionalCreditCardEntity.map(creditCardEntity -> validateCreditCard(creditCardEntity, cardType, validThru, cvv))
                .orElse(INVALID);
    }

    private String validateCreditCard(CreditCardEntity creditCardEntity, String cardType, String validThru, String cvv) {
        boolean storedDataIsTheSame = isStoredDataIsTheSame(creditCardEntity, cardType, validThru);
        boolean enabled = isCardEnabled(creditCardEntity.isDisabled());
        boolean hasIsTheSame = isHashTheSame(creditCardEntity.getCardNumber(), validThru, cvv, creditCardEntity.getHash());
        return storedDataIsTheSame && enabled && hasIsTheSame ? VALID : INVALID;
    }

    private boolean isStoredDataIsTheSame(CreditCardEntity creditCardEntity, String cardType, String validThru) {
        return cardType.equals(creditCardEntity.getCardType()) && validThru.equals(creditCardEntity.getValidThru());
    }

    private boolean isCardEnabled(boolean disabled) {
        return !disabled;
    }

    private boolean isHashTheSame(String cardNumber, String validThru, String cvv, int storedHash) {
        int requestHash = creditCardHashGenerator.generate(cardNumber, validThru, cvv);
        return storedHash == requestHash;
    }
}
