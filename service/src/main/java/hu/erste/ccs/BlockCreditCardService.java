package hu.erste.ccs;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.erste.ccs.domain.CreditCardEntity;
import hu.erste.ccs.repository.CreditCardRepository;

/**
 * Service for blocking a credit card.
 */
public class BlockCreditCardService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BlockCreditCardService.class);

    private final CreditCardRepository creditCardRepository;

    BlockCreditCardService(CreditCardRepository creditCardRepository) {
        this.creditCardRepository = creditCardRepository;
    }

    /**
     * Blocks a credit card based on the provided card number.
     *
     * @param cardNumber The number of the credit card
     */
    public void block(String cardNumber) {
        LOGGER.info("BlockCreditCardService block has been called.");
        Optional<CreditCardEntity> optionalCreditCardEntity = creditCardRepository.findByCardNumber(cardNumber);
        if (optionalCreditCardEntity.isPresent()) {
            blockCreditCard(optionalCreditCardEntity.get());
        } else {
            throw new EntityNotFoundException();
        }
    }

    private void blockCreditCard(CreditCardEntity creditCardEntity) {
        creditCardEntity.setDisabled(true);
        creditCardRepository.save(creditCardEntity);
    }
}
