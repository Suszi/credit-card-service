package hu.erste.ccs.transformer;

import static org.easymock.EasyMock.expect;
import static org.testng.Assert.assertEquals;

import java.util.Collections;
import java.util.List;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import hu.erste.ccs.domain.ContactInfo;
import hu.erste.ccs.domain.ContactInfoEntity;
import hu.erste.ccs.domain.CreditCard;
import hu.erste.ccs.domain.CreditCardEntity;

/**
 * Unit test for {@link CreditCardTransformer}.
 */
public class CreditCardTransformerTest {
    public static final String CARD_NUMBER = "123456789";
    public static final String CARD_TYPE = "cardType";
    public static final boolean DISABLED = true;
    public static final List<ContactInfoEntity> CONTACT_INFO_ENTITIES = Collections.singletonList(new ContactInfoEntity());
    public static final int HASH = 987654321;
    public static final String OWNER = "owner";
    public static final String VALID_THRU = "validThru";
    public static final List<ContactInfo> CONTACT_INFOS = Collections.singletonList(new ContactInfo("type", "contact"));

    private IMocksControl control;
    private CreditCardTransformer underTest;
    private ContactInfoTransformer contactInfoTransformer;

    @BeforeClass
    private void beforeClass() {
        control = EasyMock.createStrictControl();
        contactInfoTransformer = control.createMock(ContactInfoTransformer.class);
        underTest = new CreditCardTransformer(contactInfoTransformer);
    }

    @BeforeMethod
    private void beforeMethod() {
        control.reset();
    }

    @Test
    public void testTransformShouldReturnWithProperResponseWhenCalled() {
        // GIVEN
        CreditCardEntity creditCardEntity = createCreditCardEntity();
        expect(contactInfoTransformer.transform(CONTACT_INFO_ENTITIES)).andReturn(CONTACT_INFOS);
        control.replay();
        // WHEN
        CreditCard actual = underTest.transform(creditCardEntity);
        // THEN
        control.verify();
        assertEquals(actual.getCardNumber(), CARD_NUMBER);
        assertEquals(actual.getCardType(), CARD_TYPE);
        assertEquals(actual.getContactInfo(), CONTACT_INFOS);
        assertEquals(actual.isDisabled(), DISABLED);
        assertEquals(actual.getHash(), HASH);
        assertEquals(actual.getOwner(), OWNER);
        assertEquals(actual.getValidThru(), VALID_THRU);
    }

    private CreditCardEntity createCreditCardEntity() {
        CreditCardEntity creditCardEntity = new CreditCardEntity();
        creditCardEntity.setCardNumber(CARD_NUMBER);
        creditCardEntity.setCardType(CARD_TYPE);
        creditCardEntity.setContactInfo(CONTACT_INFO_ENTITIES);
        creditCardEntity.setDisabled(DISABLED);
        creditCardEntity.setHash(HASH);
        creditCardEntity.setOwner(OWNER);
        creditCardEntity.setValidThru(VALID_THRU);
        return creditCardEntity;
    }
}
