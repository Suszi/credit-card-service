package hu.erste.ccs.transformer;

import static org.testng.Assert.assertEquals;

import java.util.Collections;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import hu.erste.ccs.domain.ContactInfo;
import hu.erste.ccs.domain.ContactInfoEntity;

/**
 * Unit test for {@link ContactInfoTransformer}.
 */
public class ContactInfoTransformerTest {
    public static final String CONTACT = "contact";
    public static final String TYPE = "type";
    public static final int EXPECTED_SIZE = 1;

    private ContactInfoTransformer underTest;

    @BeforeClass
    private void beforeClass() {
        underTest = new ContactInfoTransformer();
    }

    @Test
    public void testTransformShouldReturnWithProperResponseWhenCalled() {
        // GIVEN
        List<ContactInfoEntity> contactInfoEntities = createContactInfoEntities();
        // WHEN
        List<ContactInfo> actual = underTest.transform(contactInfoEntities);
        // THEN
        assertEquals(actual.size(), EXPECTED_SIZE);
        ContactInfo contactInfo = actual.get(0);
        assertEquals(contactInfo.getContact(), CONTACT);
        assertEquals(contactInfo.getType(), TYPE);
    }

    private List<ContactInfoEntity> createContactInfoEntities() {
        ContactInfoEntity contactInfoEntity = new ContactInfoEntity();
        contactInfoEntity.setContact(CONTACT);
        contactInfoEntity.setType(TYPE);
        return Collections.singletonList(contactInfoEntity);
    }
}
