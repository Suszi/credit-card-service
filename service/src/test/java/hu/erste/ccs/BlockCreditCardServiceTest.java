package hu.erste.ccs;

import static org.easymock.EasyMock.expect;
import static org.testng.Assert.assertTrue;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import hu.erste.ccs.domain.CreditCardEntity;
import hu.erste.ccs.repository.CreditCardRepository;

/**
 * Unit test for {@link BlockCreditCardService}.
 */
public class BlockCreditCardServiceTest {
    public static final String CARD_NUMBER = "123456789";
    public static final boolean ENABLED = false;
    private IMocksControl control;
    private BlockCreditCardService underTest;
    private CreditCardRepository creditCardRepository;

    @BeforeClass
    private void beforeClass() {
        control = EasyMock.createStrictControl();
        creditCardRepository = control.createMock(CreditCardRepository.class);
        underTest = new BlockCreditCardService(creditCardRepository);
    }

    @BeforeMethod
    private void beforeMethod() {
        control.reset();
    }

    @AfterMethod
    private void afterMethod() {
        control.verify();
    }

    @Test
    public void testBlockShouldMarkCreditCardAsBlockedWhenCalled() {
        // GIVEN
        CreditCardEntity creditCardEntity = createCreditCardEntity();
        expect(creditCardRepository.findByCardNumber(CARD_NUMBER)).andReturn(Optional.of(creditCardEntity));
        expect(creditCardRepository.save(creditCardEntity)).andReturn(creditCardEntity);
        control.replay();
        // WHEN
        underTest.block(CARD_NUMBER);
        // THEN
        assertTrue(creditCardEntity.isDisabled());
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void testBlockShouldThrowEntityNotFoundExceptionWhenCreditCardEntityCannotBeFound() {
        // GIVEN
        expect(creditCardRepository.findByCardNumber(CARD_NUMBER)).andReturn(Optional.empty());
        control.replay();
        // WHEN
        underTest.block(CARD_NUMBER);
        // THEN exception thrown
    }

    private CreditCardEntity createCreditCardEntity() {
        CreditCardEntity creditCardEntity = new CreditCardEntity();
        creditCardEntity.setDisabled(ENABLED);
        return creditCardEntity;
    }
}
