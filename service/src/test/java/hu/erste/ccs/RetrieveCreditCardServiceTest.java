package hu.erste.ccs;

import static org.easymock.EasyMock.expect;
import static org.testng.Assert.assertEquals;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import hu.erste.ccs.domain.CreditCard;
import hu.erste.ccs.domain.CreditCardEntity;
import hu.erste.ccs.repository.CreditCardRepository;
import hu.erste.ccs.transformer.CreditCardTransformer;

/**
 * Unit test for {@link RetrieveCreditCardService}.
 */
public class RetrieveCreditCardServiceTest {
    public static final String CARD_NUMBER = "12345679";
    public static final CreditCardEntity CREDIT_CARD_ENTITY = new CreditCardEntity();
    public static final CreditCard CREDIT_CARD = new CreditCard.Builder().build();
    private IMocksControl control;
    private RetrieveCreditCardService underTest;
    private CreditCardRepository creditCardRepository;
    private CreditCardTransformer creditCardTransformer;

    @BeforeClass
    private void beforeClass() {
        control = EasyMock.createStrictControl();
        creditCardRepository = control.createMock(CreditCardRepository.class);
        creditCardTransformer = control.createMock(CreditCardTransformer.class);
        underTest = new RetrieveCreditCardService(creditCardRepository, creditCardTransformer);
    }

    @BeforeMethod
    private void beforeMethod() {
        control.reset();
    }

    @AfterMethod
    private void afterMethod() {
        control.verify();
    }

    @Test
    public void testRetrieveShouldReturnWithProperCreditCardWhenCalled() {
        // GIVEN
        expect(creditCardRepository.findByCardNumber(CARD_NUMBER)).andReturn(Optional.of(CREDIT_CARD_ENTITY));
        expect(creditCardTransformer.transform(CREDIT_CARD_ENTITY)).andReturn(CREDIT_CARD);
        control.replay();
        // WHEN
        CreditCard actual = underTest.retrieve(CARD_NUMBER);
        // THEN
        assertEquals(actual, CREDIT_CARD);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void testRetrieveShouldThrowEntityNotFoundExceptionWhenCreditCardEntityCannotBeFound() {
        // GIVEN
        expect(creditCardRepository.findByCardNumber(CARD_NUMBER)).andReturn(Optional.empty());
        control.replay();
        // WHEN
        underTest.retrieve(CARD_NUMBER);
        // THEN exception thrown
    }
}
