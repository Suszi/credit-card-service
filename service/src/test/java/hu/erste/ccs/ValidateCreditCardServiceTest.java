package hu.erste.ccs;

import static org.easymock.EasyMock.expect;
import static org.testng.Assert.assertEquals;

import java.util.Optional;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import hu.erste.ccs.domain.CreditCardEntity;
import hu.erste.ccs.repository.CreditCardRepository;

/**
 * Unit test for {@link ValidateCreditCardService}.
 */
public class ValidateCreditCardServiceTest {
    public static final String CARD_TYPE = "cardType";
    public static final String CARD_NUMBER = "cardNumber";
    public static final String CVV = "cvv";
    public static final String VALID = "VALID";
    public static final String VALID_THRU = "validThru";
    public static final boolean ENABLED = false;
    public static final int HASH = 123456798;
    public static final String INVALID = "INVALID";
    public static final int OTHER_HASH = 987654321;
    public static final String OTHER_CARD_TYPE = "otherCardType";
    public static final boolean DISABLED = true;
    public static final String OTHER_VALID_THRU = "otherValidThru";

    private IMocksControl control;
    private ValidateCreditCardService underTest;
    private CreditCardRepository creditCardRepository;
    private CreditCardHashGenerator creditCardHashGenerator;

    @BeforeClass
    private void beforeClass() {
        control = EasyMock.createStrictControl();
        creditCardRepository = control.createMock(CreditCardRepository.class);
        creditCardHashGenerator = control.createMock(CreditCardHashGenerator.class);
        underTest = new ValidateCreditCardService(creditCardRepository, creditCardHashGenerator);
    }

    @BeforeMethod
    private void beforeMethod() {
        control.reset();
    }

    @AfterMethod
    private void afterMethod() {
        control.verify();
    }

    @Test
    public void testValidateShouldReturnWithValidWhenInputDataIsValid() {
        // GIVEN
        CreditCardEntity creditCardEntity = createCreditCardEntity(ENABLED);
        expect(creditCardRepository.findByCardNumber(CARD_NUMBER)).andReturn(Optional.of(creditCardEntity));
        expect(creditCardHashGenerator.generate(CARD_NUMBER, VALID_THRU, CVV)).andReturn(HASH);
        control.replay();
        // WHEN
        String actual = underTest.validate(CARD_TYPE, CARD_NUMBER, VALID_THRU, CVV);
        // THEN
        assertEquals(actual, VALID);
    }

    @Test(dataProvider = "provideInvalidInputData")
    public void testValidateShouldReturnWithInvalidWhenValidationFailed(CreditCardEntity creditCardEntity, int hash, String cardType, String validThru) {
        // GIVEN
        expect(creditCardRepository.findByCardNumber(creditCardEntity.getCardNumber())).andReturn(Optional.of(creditCardEntity));
        expect(creditCardHashGenerator.generate(creditCardEntity.getCardNumber(), validThru, CVV)).andReturn(hash);
        control.replay();
        // WHEN
        String actual = underTest.validate(cardType, creditCardEntity.getCardNumber(), validThru, CVV);
        // THEN
        assertEquals(actual, INVALID);
    }

    @DataProvider
    private Object[][] provideInvalidInputData() {
        return new Object[][]{
                {createCreditCardEntity(DISABLED), HASH, CARD_TYPE, VALID_THRU},
                {createCreditCardEntity(ENABLED), OTHER_HASH, CARD_TYPE, VALID_THRU},
                {createCreditCardEntity(ENABLED), HASH, OTHER_CARD_TYPE, VALID_THRU},
                {createCreditCardEntity(ENABLED), HASH, CARD_TYPE, OTHER_VALID_THRU}
        };
    }

    @Test
    public void testValidateShouldReturnWithInvalidWhenCreditCardEntityCannotBeFound() {
        // GIVEN
        expect(creditCardRepository.findByCardNumber(CARD_NUMBER)).andReturn(Optional.empty());
        control.replay();
        // WHEN
        String actual = underTest.validate(CARD_TYPE, CARD_NUMBER, VALID_THRU, CVV);
        // THEN
        assertEquals(actual, INVALID);
    }

    private CreditCardEntity createCreditCardEntity(boolean disabled) {
        CreditCardEntity creditCardEntity = new CreditCardEntity();
        creditCardEntity.setDisabled(disabled);
        creditCardEntity.setCardNumber(CARD_NUMBER);
        creditCardEntity.setHash(HASH);
        creditCardEntity.setCardType(CARD_TYPE);
        creditCardEntity.setValidThru(VALID_THRU);
        return creditCardEntity;
    }
}
