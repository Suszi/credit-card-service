package hu.erste.ccs;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test for {@link CreditCardHashGenerator}.
 */
public class CreditCardHashGeneratorTest {
    public static final int EXPECTED_HASH = 706503078;
    public static final String CARD_NUMBER = "4440967484181607";
    public static final String VALID_THRU = "12/24";
    public static final String CVV = "123";

    private CreditCardHashGenerator underTest;

    @BeforeClass
    private void beforeClass() {
        underTest = new CreditCardHashGenerator();
    }

    @Test
    public void testGenerateShouldReturnWithProperHashWhenCalled() {
        // GIVEN
        // WHEN
        int actual = underTest.generate(CARD_NUMBER, VALID_THRU, CVV);
        // THEN
        assertEquals(actual, EXPECTED_HASH);
    }
}
