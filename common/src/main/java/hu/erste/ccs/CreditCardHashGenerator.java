package hu.erste.ccs;

import java.util.Arrays;

/**
 * Hash code service for credit card.
 */
public class CreditCardHashGenerator {

    /**
     * Generates hash code based on the input parameters.
     *
     * @param cardNumber The number of the credt card
     * @param validThru  The validation date of the credt card
     * @param cvv        The cvv of the credt card
     * @return The hash code of the credt card
     */
    public int generate(String cardNumber, String validThru, String cvv) {
        String[] strings = {cardNumber, validThru, cvv};
        return Arrays.hashCode(strings);
    }
}
